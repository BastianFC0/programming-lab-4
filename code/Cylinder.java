import java.lang.Math;
public class Cylinder {
    private double height;
    private double radius;
public Cylinder(double height, double radius){
    if (height<0){
    throw new IllegalArgumentException ("Negative Input");
} 
this.height = height;
if (radius<0){
    throw new IllegalArgumentException ("Negative Input");
}
this.radius = radius;
} 
public double getHeight(){
    return this.height;
}
public double getRadius(){
    return this.radius;
}
public double getVolume(){
    return Math.PI*Math.pow(this.radius, 2)*this.height;
}
public double getSurfaceArea(){
    return 2*Math.PI*Math.pow(this.radius, 2)+2*Math.PI*this.radius*this.height;
}
}