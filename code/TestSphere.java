//Bastian Fernandez Cortez
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestSphere {
    @Test
    public void tesGetRadius(){
        Sphere s = new Sphere(3.00);
        assertEquals(3.00, s.getRadius());
    }
    @Test
    public void testGetVolume(){
        Sphere s = new Sphere(3.00);
        assertEquals(84.82300, s.getVolume(),.00001);

    }
    @Test
    public void testGetSurfaceArea(){
        Sphere s = new Sphere(3.00);
        assertEquals(113.09733, s.getSurfaceArea(),.00001);
    }
    @Test
    public void testNegativeInput(){
        try{
        Sphere s = new Sphere(-3.00);
        }
        catch(IllegalArgumentException e){
            System.out.println("Negative input");
        };
        };
    }
