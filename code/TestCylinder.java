//Oscar Palencia
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TestCylinder {
   
    @Test
    public void testDataValidation() {
        try {
            Cylinder c = new Cylinder(-5.0, 5.0);
            assertEquals(5.0, c.getHeight());
        }
        catch(Exception e) {
            Cylinder c = new Cylinder(5.0, 5.0);
            assertEquals(5.0, c.getHeight());
        }
    }

    @Test
    public void testGetHeight() {
        Cylinder c = new Cylinder(10.0, 5.0);
        assertEquals(10.0, c.getHeight());
    }

    @Test
    public void testGetRadius() {
        Cylinder c = new Cylinder(10.0, 5.0);
        assertEquals(5.0, c.getRadius());
    }

    @Test
    public void testGetVolume() {
        Cylinder c = new Cylinder(10.0, 5.0);
        assertEquals(785.4, c.getVolume(), 0.1);
    }

    @Test
    public void testGetSurfaceArea() {
        Cylinder c = new Cylinder(10.0, 5.0);
        assertEquals(471.24, c.getSurfaceArea(), 0.01);
    }
}
