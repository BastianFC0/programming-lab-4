public class Sphere {
    private double radius;

    public Sphere(double r) {
        if(r < 0) {
            throw new IllegalArgumentException("radius cannot be a negative number");
        }
        this.radius = r;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getVolume() {
        return Math.PI * Math.pow(this.radius, 3);
    }

    public double getSurfaceArea() {
        return 4 * Math.PI * Math.pow(this.radius, 2);
    }
}
